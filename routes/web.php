<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

/**
 * Display all cards
 */
Route::get('/cards', 'CardController@showAll') ->name('cards.overview');
Route::post('/cards', 'CardController@filter') ->name('cards.filter');

/**
 * Upload Card manually
 */
Route::get('/cards/add', 'CardController@addform') ->name('addcardform');
Route::post('/cards/add', 'CardController@addCard') ->name('cards.add');

/**
 * Upload Dice manually
 */
Route::get('/dice/add/{cardid}/{cardname}', 'DicefaceController@index') ->name('diceindex');
Route::post('/dice/add/', 'DicefaceController@addDice') ->name('dice.add');

/**
 * Upload Card excel
 */
Route::get('/cards/upload','CardController@index') ->name('index');
Route::post('/cards/upload','CardController@import') ->name('upload');

/**
 * Home Screen
 */
Route::get('/', function() {
    $icons = array("a", "b", "c", "d", "e", "f", "g");    
    return view('welcome', ['icon' => $icons[ array_rand( $icons, 1) ]]);
}) ->name('welcome');