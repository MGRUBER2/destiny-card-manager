<?php

use Faker\Generator as Faker;



$factory->define(App\Card::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text(100),
        'cardtype_id' => $faker->randomElement(App\Cardtype::pluck('id')->toArray()),
        'color_id' => $faker->randomElement(App\Color::pluck('id')->toArray()),
        'serie_id' => $faker->randomElement(App\Serie::pluck('id')->toArray()),
        'fraction_id' => $faker->randomElement(App\Fraction::pluck('id')->toArray()),
        'unique' => $faker->boolean,
        'cost' => $faker->numberBetween(1,6),
        'cardnumber' => $faker->numberBetween(001, 500)
    ];
});
