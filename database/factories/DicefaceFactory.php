<?php

use Faker\Generator as Faker;

$factory->define(App\Diceface::class, function (Faker $faker) {
    
    return [
        'dicefacetype_id' => $faker->randomElement(App\Dicefacetype::pluck('id')->toArray()),
        'amount' => $faker->numberBetween(1,6),
        'cost' => $faker->numberBetween(0,5),
        'isModifier' => $faker->boolean(),
        'card_id' => $faker->randomElement(App\Card::pluck('id')->toArray())
    ];

});
