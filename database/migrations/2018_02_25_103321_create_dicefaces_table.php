<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiceFacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dicefaces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dicefacetype_id')->references('id')->on('dicefacetypes');
            $table->boolean('isModifier')->default(false);
            $table->integer('amount')->nullable();
            $table->integer('cost')->nullable();
            $table->integer('card_id')->references('id')->on('cards');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dicefaces');
    }
}
