<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->integer('cardtype_id')->references('id')->on('cardtypes');
            $table->integer('fraction_id')->references('id')->on('fractions');
            $table->integer('color_id')->references('id')->on('colors');
            $table->integer('stamina')->nullable();
            $table->string('special')->nullable();
            $table->boolean('unique')->default(false);
            $table->integer('serie_id')->references('id')->on('series');
            $table->integer('cost');
            $table->integer('cardnumber');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
