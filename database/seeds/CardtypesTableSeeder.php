<?php

use Illuminate\Database\Seeder;

class CardtypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cardtypes')->insert([
            ['name' => 'Charakter'],
            ['name' => 'Unterstützung'],
            ['name' => 'Fahrzeug'],
            ['name' => 'Ausrüstung'],                         
            ['name' => 'Droide'],
            ['name' => 'Fähigkeit'],                           
            ['name' => 'Ereignis'],
            ['name' => 'Schlachtfeld'],
        ]);
    }
}
