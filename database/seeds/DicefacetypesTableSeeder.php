<?php

use Illuminate\Database\Seeder;

class DicefacetypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dicefacetypes')->insert([
            ['name' => 'meele'],                     
            ['name' => 'ranged'],                     
            ['name' => 'blank'],                     
            ['name' => 'undefined'],                     
            ['name' => 'indirect'],   
            ['name' => 'special'],
            ['name' => 'disrupt'], 
            ['name' => 'pullcard'],
            ['name' => 'credits'],
            ['name' => 'shield']                                                                                                                                                                                                                                                                                      
        ]);
    }
}
