<?php

use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->insert([
            ['name' => 'Neutral'],                     
            ['name' => 'Rot'],                     
            ['name' => 'Blau'],                     
            ['name' => 'Gelb']
        ]);
    }
}
