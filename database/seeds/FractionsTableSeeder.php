<?php

use Illuminate\Database\Seeder;

class FractionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fractions')->insert([
            ['name' => 'Neutral'],                     
            ['name' => 'Held'],                     
            ['name' => 'Schurke'],                     
        ]);
    }
}
