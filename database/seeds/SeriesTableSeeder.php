<?php

use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('series')->insert([
            ['name' => 'Spirit of the Rebellion', 'shortname'=> 'SoR'],
            ['name' => 'Empire at War', 'shortname'=> 'EaW'],
            ['name' => 'Awakening', 'shortname'=> 'Awakening'],
            ['name' => 'Legacies', 'shortname'=> 'Legacies'],                         
        ]);
    }
}
