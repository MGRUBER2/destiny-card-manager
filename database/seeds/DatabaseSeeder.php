<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DicefacetypesTableSeeder::class);
        $this->call(ColorsTableSeeder::class);
        $this->call(FractionsTableSeeder::class);
        $this->call(SeriesTableSeeder::class);
        $this->call(CardtypesTableSeeder::class);
        $this->call(CardsTableSeeder::class);
        $this->call(DicefaceTableSeeder::class);
    }
}
