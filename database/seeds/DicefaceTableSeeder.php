<?php

use Illuminate\Database\Seeder;
use App\Diceface;

class DicefaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Diceface::class, 100)->create();
    }
}
