<!DOCTYPE html>
<html lang="en" lang="{{ app()->getLocale() }}">
    @include('partials.head');

    <div class="container" id="welcome">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="logo">
                    <span data-icon="{{ $icon }}" class="icon"></span>
                </div>
            </div>
        </div>
    </div>
    
</html>