@extends('layouts.app')

@section('content')
<div class="container">

    <h1 class="text-center"> 
        Schritt 2: {{ $cardname }} Würfel erfassen 
    </h1>

    <hr>

    <h2> Würfel </h2>

    @if ( Session::has('success') )
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    <hr>

    <!-- Würfel Formular -->
    {!! Form::open(['route' => 'dice.add', 'method' => 'post' , 'id' => 'adddice']) !!}
    {{ csrf_field() }}

    @for ($i =1; $i <= 6; $i++)
    <div class="row">
        <h3>Seite {{ $i }} </h3>
        {!! Form::hidden('card_id', $card_id ) !!};

        <div class="form-group col-md-2">
            {!! Form::label('dicefacetype_id', 'Typ:') !!}                   
            {!! Form::select('dicefacetype_id', $dicefacetypes, null, ['class' => 'form-control', 'placeholder' => 'Wähle eine Würfelseite', 'required' => 'required']) !!}  
        </div>
 
        <div class="form-group col-md-2">
            {!! Form::label('amount', 'Wert:') !!}
            {!! Form::number('amount', 0, ['class' => 'form-control']) !!}
        </div>
 
        <div class="form-group col-md-2">
                {!! Form::label('cost', 'Kosten:') !!}
                {!! Form::number('cost', 0, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-md-2">                
                {!! Form::label('isModifier', 'Modifikator') !!}
                {!! Form::checkbox('isModifier', 1, false, ['class' => 'form control']) !!}
        </div>
    </div>
    @endfor

    {!! Form::submit('Würfel erfassen', ['class' => 'btn btn-large btn-primary']) !!}

    {!! Form::close() !!}

</div>
@endsection('content')