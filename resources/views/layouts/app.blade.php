<!DOCTYPE html>
<html lang="en" lang="{{ app()->getLocale() }}">

    @include('partials.head');

    <body>
        <div id="navigation" class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    @include('partials.navigation')
                </div>
            </div>
        </div>
        
        <div id="wrapper" class="container">
            @yield('content')
        </div>

        <footer id="footer" class="page-footer font-small blue pt-4 mt-4">
            <div clasS="container">
                @include('partials.footer')
            </div>
        </footer>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </body>

</html>