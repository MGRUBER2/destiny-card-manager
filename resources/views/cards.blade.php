@extends('layouts.app')

@section('content')

@include('partials.card-filter')

<div class="component cardoverview">
    
    <div class="panel panel-default">
    
        <div class="panel-heading">
            <h1>Karten Übersicht:</h1>
        </div>
    
        <div class="panel-body">
            <div class="row">
                @foreach($cards as $key => $card)
                    <div class="col-xs-6 col-md-3">
                        <div class="card {{$card->color->name}}">
                            
                            <img class="card-img-top" src={{ asset('images/cards/' . $card->serie->shortname . '/' . $card->cardnumber . '.jpg') }} />    
                            
                            <div class="card-body">
                                <h5 class="card-title">{{$card->unique}} {{$card->name}}</h5>
                                <p>{{$card->cardnumber}}</p>
                            </div>
                        </div>
                    </div>            
                @endforeach
            </div>
            <div class="row">
                {{ $cards->links() }}
            </div>
        </div>

    </div>
</div>

@endsection