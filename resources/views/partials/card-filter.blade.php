<div class="panel panel-default">
    <div class="panel-heading">Karten Filter:</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'cards.filter', 'method' => 'post' , 'id' => 'cardfilter']) !!}
                    <div class="row">
                        <div class="form-group col-md-2">
                            {!! Form::label('cardnumber', 'Katennummer:') !!}
                            {!! Form::number('cardnumber', null, ['class' => 'form-control']) !!}
                        </div>
                        
                        <div class="form-group col-md-2">
                            {!! Form::label('series', 'Serie:') !!}
                            {!! Form::select('series', $series, null, ['class' => 'form-control', 'placeholder' => 'Wähle eine Serie']) !!}                
                        </div>

                        <div class="form-group col-md-2">
                            {!! Form::label('fraction', 'Fraktion:') !!}                   
                            {!! Form::select('fraction', $fractions, null, ['class' => 'form-control', 'placeholder' => 'Wähle eine Fraktion']) !!}  
                        </div>
                        
                        <div class="form-group col-md-2">
                            {!! Form::label('color', 'Farbe:') !!}                   
                            {!! Form::select('color', $colors, null, ['class' => 'form-control', 'placeholder' => 'Wähle eine Farbe']) !!}  
                        </div>

                        <div class="form-group col-md-2">                
                            {!! Form::label('cardtype', 'Kartentyp:') !!}                   
                            {!! Form::select('cardtype', $cardtypes, null, ['class' => 'form-control', 'placeholder' => 'Wähle einen Kartentyp']) !!}  
                        </div>

                        <div class="form-group col-md-2">                
                            {!! Form::label('dicefacetype', 'Würfelseite:') !!}                   
                            {!! Form::select('dicefacetype', $dicefacetypes, null, ['class' => 'form-control', 'placeholder' => 'Wähle eine Würfelseite']) !!}  
                        </div>

                        <div class="form-group col-md-2">                
                            {!! Form::label('hasDice', 'Mit Würfel') !!}
                            {!! Form::checkbox('hasDice', false, null, ['class' => 'form control']) !!}
                        </div>
                    </div>

                    {!! Form::submit('Filter', ['class' => 'btn btn-large btn-primary']) !!}

                    <button class="btn btn-link" type="button" onclick="window.location='{{ route("cards.overview") }}'">Alle filter zurücksetzen</button>

                {!! Form::close() !!}

                <hr>
            </div>
        </div>
</div>