<div class="logo">
    <span style="font-size: 2rem; margin-bottom: 2rem" data-icon="f" class="icon"></span>
</div>

<nav class="navbar navbar-expand-sm bg-light">

    <!-- Links -->
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="{{ url('/cards') }}">Karten</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ url('/cards/add') }}">Karte hinzufügen</a>
    </li>

    </ul>
</nav>