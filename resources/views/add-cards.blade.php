@extends('layouts.app')

@section('content')
<div class="container">

    <h1 class="text-center">
        Schritt 1: Neue Karte erfassen
    </h1>

    <hr>

    <h2> Karte </h2>


    @if ( Session::has('success') )
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        <div>
            @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
            @endforeach
        </div>
    </div>
    @endif

    {!! Form::open(['route' => 'cards.add', 'method' => 'post' , 'id' => 'addcard']) !!}
    
    <div class="row">
        {{ csrf_field() }}

        <div class="form-group col-md-12">
                {!! Form::label('name', 'Kartenname:') !!}
                {!! Form::text('name', null, ['class' => 'form-control input-lg', 'required' => 'required']) !!}
        </div>

        <div class="form-group col-md-12">
                {!! Form::label('description', 'Kartenbeschreibung:') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>
        
        <div class="form-group col-md-12">
                {!! Form::label('special', 'Spezialfähigkeiten:') !!}
                {!! Form::textarea('special', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-md-2">
                {!! Form::label('serie_id', 'Serie:') !!}
                {!! Form::select('serie_id', $series, null, ['class' => 'form-control', 'placeholder' => 'Wähle eine Serie', 'required' => 'required']) !!}                
        </div>

        <div class="form-group col-md-2">
                {!! Form::label('cardnumber', 'Kartennummer:') !!}
                {!! Form::number('cardnumber', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-md-2">
            {!! Form::label('fraction_id', 'Fraktion:') !!}                   
            {!! Form::select('fraction_id', $fractions, null, ['class' => 'form-control', 'placeholder' => 'Wähle eine Fraktion', 'required' => 'required']) !!}  
        </div>

        <div class="form-group col-md-2">
            {!! Form::label('color_id', 'Farbe:') !!}                   
            {!! Form::select('color_id', $colors, null, ['class' => 'form-control', 'placeholder' => 'Wähle eine Farbe', 'required' => 'required']) !!}  
        </div>

        <div class="form-group col-md-2">                
            {!! Form::label('cardtype_id', 'Kartentyp:') !!}                   
            {!! Form::select('cardtype_id', $cardtypes, null, ['class' => 'form-control', 'placeholder' => 'Wähle einen Kartentyp', 'required' => 'required']) !!}  
        </div>

        <div class="form-group col-md-2">
                {!! Form::label('cost', 'Kosten:') !!}
                {!! Form::number('cost', 0, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-md-2">
                {!! Form::label('stamina', 'Lebenspunkte :') !!}
                {!! Form::number('stamina', null, ['class' => 'form-control']) !!}
        </div>
        
        <div class="form-group col-md-2">                
                {!! Form::label('unique', 'Einzigartig') !!}
                {!! Form::checkbox('unique', 1, false, ['class' => 'form control']) !!}
        </div>

        <div class="form-group col-md-2">                
                {!! Form::label('hasdice', 'Hat Würfel') !!}
                {!! Form::checkbox('hasdice', 1, false, ['class' => 'form control']) !!}
        </div>

    </div>

    {!! Form::submit('Karte erfassen', ['class' => 'btn btn-large btn-primary']) !!}

    {!! Form::close() !!}

</form>
</div>
@endsection('content')