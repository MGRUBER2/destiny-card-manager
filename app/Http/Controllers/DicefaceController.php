<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dicefacetype;
use App\Diceface;

class DicefaceController extends Controller
{
    public function index($cardid, $cardname) {
        $dicefacetypes = Dicefacetype::pluck('name', 'id');
        return view('add-dice',['card_id' => $cardid, 'cardname' => $cardname, 'dicefacetypes' => $dicefacetypes ]);    
    }

    public function addDice(Request $request) {
        $diceface = Diceface::create($request->all());
    
        try {
            $request->session()->flash('success', 'Würfel erfolgreich hinzugefügt');
        } catch (\Exception $e) {
            $request->session()->flash('error', 'Etwas ist leider schiefgelaufen');
        }

        return back();

    }
}
