<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Session;
use Excel;
use File;

use App\Card;
use App\Serie;
use App\Fraction;
use App\Color;
use App\Cardtype;
use App\Dicefacetype;


class CardController extends Controller
{

    public function index() {
        return view('add-cards');
    }

    /**
     * Add cards GET
     */
    public function addform() {

        $series = Serie::pluck('name', 'id');
        $fractions = Fraction::pluck('name', 'id');
        $colors = Color::pluck('name', 'id');
        $cardtypes = Cardtype::pluck('name', 'id');
        $dicefacetypes = Dicefacetype::pluck('name', 'id');
        
        return view('add-cards', [  'series' => $series, 
                                'colors' => $colors, 
                                'fractions' => $fractions, 
                                'cardtypes' => $cardtypes,
                                'dicefacetypes' => $dicefacetypes
                            ] );  
    }

     /**
     * Add Cards POST
     */
    public function addCard(Request $request) {
        
        // Validation
        $validated_data = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'cardnumber' => 'required',
            'cardtype_id' => 'required',
            'color_id' => 'required',
            'serie_id' => 'required',
            'cost' => 'required'
        ]);
        
        // Check if card already exists
        $duplicationCount = Card::where('cardnumber', $request->cardnumber)
                                ->where('serie_id', $request->serie_id)
                                ->count();

        if($duplicationCount > 0) {
            $request->session()->flash('error', 'Duplikat: Diese Karte existiert bereits');
            return back();
        }

        $card = Card::create($request->all());
                        
        // Check if dice exists
        if($request->hasdice) {          
            return redirect()->route('diceindex', ['cardid' => $card->id, 'cardname'=>$card->name]);
        } else {

            try {
                $request->session()->flash('success', 'Karte erfolgreich hinzugefügt');
            } catch (\Exception $e) {
                $request->session()->flash('error', 'Etwas ist leider schiefgelaufen');
            }

            return back();
        }   
    }

    /**
     * Get all cards
     */
    public function showAll() {

        /**
         * Preparing Filter data
         */
        $series = Serie::pluck('name', 'id');
        $fractions = Fraction::pluck('name', 'id');
        $colors = Color::pluck('name', 'id');
        $cardtypes = Cardtype::pluck('name', 'id');
        $dicefacetypes = Dicefacetype::pluck('name', 'id');
        $cards = Card::paginate(10);
        
        return view('cards', [  'cards' => $cards, 
                                'series' => $series, 
                                'colors' => $colors, 
                                'fractions' => $fractions, 
                                'cardtypes' => $cardtypes,
                                'dicefacetypes' => $dicefacetypes
                            ] );    
    }

    /**
     * filter cards
     */
    public function filter(Request $request) {

        $query = $request->only(['series', 'cardnumber', 'fraction', 'color', 'cardtype', 'dicefacetype']);
        $conditions = collect($query);

        /**
         * Filter conditions
         */
        $cards = Card::where(function($q) use ($conditions) {
            
            if($conditions->has('series')) {
                if(!is_null($conditions->get('series')) ) {
                    $q->where('serie_id', $conditions->get('series'));
                }
            }
            
            if($conditions->has('cardnumber')) {
                if(!is_null($conditions->get('cardnumber')) ) {
                    $q->where('cardnumber', $conditions->get('cardnumber'));
                }
            }

            if($conditions->has('fraction')) {
                if(!is_null($conditions->get('fraction')) ) {
                    $q->where('fraction_id', $conditions->get('fraction'));
                }
            }

            if($conditions->has('color')) {
                if(!is_null($conditions->get('color')) ) {
                    $q->where('color_id', $conditions->get('color'));
                }
            }

            if($conditions->has('cardtype')) {
                if(!is_null($conditions->get('cardtype')) ) {
                    $q->where('cardtype_id', $conditions->get('cardtype'));
                }
            }

            if($conditions->has('dicefacetype')) {
                if(!is_null($conditions->get('dicefacetype')) ) {
                    $q->whereHas('diceface', function($q) use ($conditions) {
                        $q->where('dicefacetype_id', $conditions->get('dicefacetype'));
                    });
                }
            }

            return $q;

        })->paginate(100);
        
        /**
         * Preparing Filter data
         */
        $series = Serie::pluck('name', 'id');
        $fractions = Fraction::pluck('name', 'id');
        $colors = Color::pluck('name', 'id');
        $cardtypes = Cardtype::pluck('name', 'id');
        $dicefacetypes = Dicefacetype::pluck('name', 'id');
        
        return view('cards', [  'cards' => $cards, 
                                'series' => $series, 
                                'colors' => $colors, 
                                'fractions' => $fractions, 
                                'cardtypes' => $cardtypes,
                                'dicefacetypes' => $dicefacetypes
                            ] );

    }

}
