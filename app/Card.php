<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    // Mass assignment
    protected $fillable = [ 'name', 
                            'description', 
                            'special', 
                            'cardnumber', 
                            'type_id', 
                            'cost',
                            'fraction_id',
                            'cardtype_id',
                            'color_id',
                            'stamina',
                            'special',
                            'unique',
                            'serie_id',
    ];

    /**
     * Dice to belongs to this Card
     */
    public function diceface() {
        return $this->hasMany('App\Diceface');
    }

    /**
     * Serie that belongs to this Card
     */
    public function serie() {
        return $this->belongsTo('App\Serie');
    }

    /**
     * Type that belongs to this Card
     */
    public function cardtype() {
        return $this->belongsTo('App\Cardtype');
    }

    /**
     * Fraction that belongs to this Card
     */
    public function fraction() {
        return $this->belongsTo('App\Fraction');
    }

    /**
     * Color that belongs to this Card
     */
    public function color() {
        return $this->belongsTo('App\Color');
    }

}
