<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cardtype extends Model
{
    //

    /**
     * Card that is Connected to this type
     */
    public function Card() {
        return $this->hasMany('App\Card');
    }
}
