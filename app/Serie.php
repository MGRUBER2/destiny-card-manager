<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
   
    /**
     * Card that is Connected to this serie
     */
    public function card() {
        return $this->hasMany('App\Card');
    }

}
