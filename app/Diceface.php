<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diceface extends Model
{
    protected $fillable = [
        'dicefacetype_id',
        'card_id',
        'amount',
        'isModifier',
        'cost'
    ];

    public function dicefacetype() {
        return $this->belongsTo('App\Dicefacetype');
    }
}
